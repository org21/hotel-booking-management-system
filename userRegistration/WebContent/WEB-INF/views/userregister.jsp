<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div align="center">
  <h1>User Register Form</h1>
  <form action="<%= request.getContextPath() %>/register" method="post">
   <table style="width: 80%">
    <tr>
     <td>First Name</td>
     <td><input type="text" name="firstName" /></td>
    </tr>
    <tr>
     <td>Last Name</td>
     <td><input type="text" name="lastName" /></td>
    </tr>
    <tr>
     <td>UserName</td>
     <td><input type="text" name="username" /></td>
    </tr>
    <tr>
     <td>Password</td>
     <td><input type="password" name="password" /></td>
    </tr>
    <tr>
     <td>Dob</td>
     <td><input type="date" name="date" /></td>
    </tr>
    
    <tr>
    <tr>
     <td>email</td>
     <td><input type="email" name="email" /></td>
    </tr>
    <tr>
     <td>Contact No</td>
     <td><input type="tel" name="contact" /></td>
    </tr>
    <tr>
     <td>State</td>
     <td><input type="text" name="state" /></td>
    </tr>
    <tr>
   </table>
   <input type="submit" value="Submit" />
  </form>
 </div>

</body>
</html>