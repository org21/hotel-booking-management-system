package com.hbms.registration.dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.hbms.registration.model.User;

public class UserDao {
	
    public int registerUser(User user) throws ClassNotFoundException 
    {

    	String INSERT_USERS_SQL = "INSERT INTO user" +
	            "  (first_name, last_name, username, password, dob,email,contact,state) VALUES " +
	            " (?, ?, ?, ?,?,?,?,?);";
	       
	        int result = 0;
	        
	        //Class.forName("com.mysql.jdbc.Driver");
	        Class.forName("com.mysql.cj.jdbc.Driver");

	        try (Connection connection = DriverManager
	            .getConnection("jdbc:mysql://localhost:3306/userregister?useSSL=false", "root", "swecha");

	            // Step 2:Create a statement using connection object
	            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL)) {
	            //preparedStatement.setInt(1, 1);
	            preparedStatement.setString(1, user.getFirstName());
	            preparedStatement.setString(2, user.getLastName());
	            preparedStatement.setString(3, user.getUsername());
	            preparedStatement.setString(4, user.getPassword());
	            preparedStatement.setString(5, user.getDate());
	            preparedStatement.setString(6, user.getEmail());
	            preparedStatement.setString(7, user.getContact());
	            preparedStatement.setString(8, user.getState());
	            System.out.println(preparedStatement);
	            // Step 3: Execute the query or update query
	            result = preparedStatement.executeUpdate();

	        } catch (SQLException e) {
	            // process sql exception
	            e.printStackTrace();
	        }
	        return result;
	    }
	/*
	    private void printSQLException(SQLException ex) {
	        for (Throwable e: ex) {
	            if (e instanceof SQLException) {
	                e.printStackTrace(System.err);
	                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
	                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
	                System.err.println("Message: " + e.getMessage());
	                Throwable t = ex.getCause();
	                while (t != null) {
	                    System.out.println("Cause: " + t);
	                    t = t.getCause();
	                }
	            }
	        }
	 */

	}

