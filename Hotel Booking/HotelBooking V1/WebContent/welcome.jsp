<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Thaj Hotel</title>
</head>
<style>
@import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,900);
body {
  background: 
    linear-gradient(
      rgba(0, 0, 0, 0.5),
      rgba(0, 0, 0, 0.5)
    ),
    url(https://www.trustyou.com/wp-content/uploads/2017/11/Umaid-Bhawan-Palace-Jodhpur-1.jpg);
  background-size: cover;
  font-family: 'Source Sans Pro', sans-serif;
}

header {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: white;
  text-align: center;
}
h1 {
  text-transform: uppercase;
  margin: 0;
  font-size: 3rem;
  white-space: nowrap;
  color:white;
}
h3{
color:white;
}
p {
  margin: 0;
  font-size: 1.5rem;
}
</style>
<body>
<marquee>
<h3> Hi ${message} , Welcome to our Thaj Hotels!!</h3>

</marquee>

<header>
  <h1>- THAJ HOTELS -</h1>
  <p>Enjoy your day</p>
</header>


</body>
</html>