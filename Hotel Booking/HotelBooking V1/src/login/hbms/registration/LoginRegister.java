package login.hbms.registration;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginRegister
 */

@WebServlet("/loginRegister")

public class LoginRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public LoginRegister() 
    {
    
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	
    	CustomerDAO cd=new CustomerDAOImpl();
    	
    	String userName=request.getParameter("username");
    	String password=request.getParameter("password1");
    	String submitType=request.getParameter("submit");
    	int status;
    	
    	System.out.println(userName+""+ password);
    	
    	Customer c=new Customer();
        c=cd.getCustomer(userName, password);
    	
    	if(submitType.equals("login") && c!=null && c.getFirstname()!=null) {

    		request.setAttribute("message",c.getFirstname());
    		request.getRequestDispatcher("welcome.jsp").forward(request, response);
    		
    	}else if(submitType.equals("register")) {
    	
    		c.setUAN(request.getParameter("IdValue"));
    		c.setUsername(userName);
    		c.setFirstname(request.getParameter("fname"));
    		c.setLastname(request.getParameter("lname"));
    		c.setPassword(password);
    		c.setEmail(request.getParameter("email"));
    		c.setContact(request.getParameter("contact"));
    		c.setState(request.getParameter("state"));
    		status=cd.insertCustomer(c);
    		System.out.println("status"+status);
    		
    		if(status==1) {
    		request.setAttribute("message", "Registration Done, Please Login to Continue !!!");
     	    request.getRequestDispatcher("login.jsp").forward(request, response);    	    
    		}
     	    else if(status==-1) {
     	    	//System.out.println("Error , Please Enter correct values");
     	    	
     	    request.setAttribute("message", "Error,Please enter correct value !!!");
     	    
        	   request.getRequestDispatcher("register.jsp").include(request, response);
        	    
     	    }
    	}else {
    	    request.setAttribute("message", "Data Not Found, Click on Register !!!");
    	    request.getRequestDispatcher("login.jsp").forward(request, response);
    	    
    	}
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
	}

}
