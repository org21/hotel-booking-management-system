package login.hbms.hotels;

public class Hotel {

	private String hotelName;
	private String gstNumber;
	private String Address;
	private String City;
	private String RoomType;
	private String RoomSize;
	private String Price;
	private String Amenities;
	private String Total_Available_Rooms;
	private String hotelId;
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getGstNumber() {
		return gstNumber;
	}
	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getRoomType() {
		return RoomType;
	}
	public void setRoomType(String roomType) {
		RoomType = roomType;
	}
	public String getRoomSize() {
		return RoomSize;
	}
	public void setRoomSize(String roomSize) {
		RoomSize = roomSize;
	}
	public String getPrice() {
		return Price;
	}
	public void setPrice(String price) {
		Price = price;
	}
	public String getAmenities() {
		return Amenities;
	}
	public void setAmenities(String amenities) {
		Amenities = amenities;
	}
	public String getTotal_Available_Rooms() {
		return Total_Available_Rooms;
	}
	public void setTotal_Available_Rooms(String total_Available_Rooms) {
		Total_Available_Rooms = total_Available_Rooms;
	}
	
}
