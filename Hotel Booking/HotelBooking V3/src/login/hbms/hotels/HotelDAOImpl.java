package login.hbms.hotels;

import java.sql.Connection;
import java.sql.PreparedStatement;

import login.hbms.registration.MyConnectionProvider;

public class HotelDAOImpl implements HotelDAO {

	static Connection con;
	static PreparedStatement ps;
	
	@Override
	public int insertHotel(Hotel h) {
       int status1=0;
       
       try {
    	con=MyConnectionProvider.getCon();
   		ps=con.prepareStatement("insert into hotels(hotel_id,hotel_name,hotel_gst_no,address,city,room_type,room_size,"
   				+ "price,amenities,total_no_of_rooms)"
    	+ "values(?,?,?,?,?,?,?,?,?,?)");
   		
   		ps.setString(1, h.getHotelId());
   		ps.setString(2, h.getHotelName());
   		ps.setString(3, h.getGstNumber());
   		ps.setString(4, h.getAddress());
   		ps.setString(5, h.getCity());
   		ps.setString(6, h.getRoomType());
   		ps.setString(7, h.getRoomSize());
   		ps.setString(8, h.getPrice());
   		ps.setString(9, h.getAmenities());
   		ps.setString(10, h.getTotal_Available_Rooms());
   		
   		status1=ps.executeUpdate();
   		con.close();
   		
       }catch(Exception e) {
    	   System.out.println(e);
       }
		return status1;
	}

}
