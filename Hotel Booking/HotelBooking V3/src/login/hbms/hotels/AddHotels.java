package login.hbms.hotels;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import login.hbms.registration.Customer;


@WebServlet(name = "addHotels", urlPatterns = { "/addHotels" })
public class AddHotels extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
     
    public AddHotels() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int status1;
		 HotelDAO hd=new HotelDAOImpl();
	    	Hotel h=new Hotel();

	     String submitType=request.getParameter("submit");
	     
	     if(submitType.equals("addhotels")) {
	    	 h.setHotelName(request.getParameter("hname"));
	    	 h.setGstNumber(request.getParameter("gst"));
	    	 h.setAddress(request.getParameter("address"));
	    	 h.setCity(request.getParameter("city"));
	    	 h.setRoomSize(request.getParameter("size"));
	    	 h.setRoomType(request.getParameter("type"));
	    	 h.setPrice(request.getParameter("price"));
	    	 h.setAmenities(request.getParameter("amenities"));
	    	 h.setTotal_Available_Rooms(request.getParameter("availablerooms"));
	    	 hd.insertHotel(h);
	    	 
	    	 
	     }	 
	}
}