package login.hbms.registration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CustomerDAOImpl implements CustomerDAO {

	static Connection con;
	static PreparedStatement ps;
	
	@Override
	public int insertCustomer(Customer c) {
		
		int status=0;
		
		try {
			//System.out.println("add Customer...");
			con=MyConnectionProvider.getCon();
			//ps = con.prepareStatement("insert into customer (first_name,last_name,username, password, name) values (?,?,?)");
	
			ps=con.prepareStatement("insert into customer2(first_name, last_name, username, password,dob,email,contact,state,UAN) VALUES (?, ?, ?, ?,?,?,?,?,?)");
			
			ps.setString(1, c.getFirstname());
			ps.setString(2, c.getLastname());
			ps.setString(3, c.getUsername());
			ps.setString(4, c.getPassword());
			ps.setString(5, c.getDateofbirth());
			ps.setString(6, c.getEmail());
			ps.setString(7, c.getContact());
			ps.setString(8, c.getState());
			ps.setString(9, c.getUAN());
			status=ps.executeUpdate();
			con.close();	
		//	System.out.println(c.getUsername() + " added");
		}catch(Exception e) {
			status=-1;
			
			System.out.println(e);
		}
		return status;
	}

	@Override
	public Customer getCustomer(String username, String pass) {
		
		Customer c=new Customer();
	
       try {
		con=MyConnectionProvider.getCon();
		ps=con.prepareStatement("select * from customer2 where username=? and password=?");
		ps.setString(1, username);
		ps.setString(2, pass);
		
		System.out.println(username+""+ pass);
		ResultSet rs=ps.executeQuery();
		while(rs.next()) 
		{
			c.setFirstname(rs.getString("first_name"));
			
			//System.out.println();
			c.setLastname(rs.getString(2));
			c.setUsername(rs.getString("username"));
			
			c.setPassword(rs.getString(4));
			c.setDateofbirth(rs.getString(5));
			c.setEmail(rs.getString(6));
			c.setContact(rs.getString(7));
			c.setState(rs.getString(8));
			c.setUAN(rs.getString(9));
		
		}
		System.out.println(c.getUsername()+c.getPassword());
		
		con.close();
		}catch(Exception e) {
			System.out.println(e);
		}	
		return c;
	}

}
