package login.hbms.guest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import login.hbms.registration.MyConnectionProvider;

public class GuestDAOImpl implements GuestDAO {
	static Connection con;
	static PreparedStatement ps;
	
	@Override
	public int insertGuest(Guest g) {
	 int status=0;
		
		try {
			//System.out.println("add Customer...");
			con=MyConnectionProvider.getCon();	
			ps=con.prepareStatement("insert into guest(booking_id,first_name, last_name,dateofbirth,email,contact) VALUES (?,?, ?, ?, ?,?)");

			ps.setString(1, g.getBookingId());
			ps.setString(2, g.getFirstname());
			ps.setString(3, g.getLastname());
			ps.setString(4, g.getDateofbirth());
			ps.setString(5, g.getEmail());
			ps.setString(6, g.getContact());
			
			status=ps.executeUpdate();
			con.close();	
			
			//	System.out.println(c.getUsername() + " added");
			}catch(Exception e) {
				status=-1;
				
				System.out.println(e);
			}
			return status;
	}
}