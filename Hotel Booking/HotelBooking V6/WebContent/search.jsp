<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
<style>


body {
  font-family: "Open Sans", sans-serif;
  line-height: 1.25;
}

table {
  border: 1px solid #ccc;
  border-collapse: collapse;
  margin: 0;
  padding: 0;
  width: 50%;
  table-layout: fixed;
}
table tr {
  background-color: #f8f8f8;
  border: 1px solid #ddd;
  padding: .25em;
}

table th,
table td {
  padding: .625em;
  text-align: center;
}
table {
  width: 70%;
  
}
	
@media screen and (max-width: 500px) {
  table {
    border: 0;
  }

  table caption {
    font-size: 1.3em;
  }

}

 @import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,900);
        body {
          background: 
            linear-gradient(
              rgba(0, 0, 0, 0.5),
              rgba(0, 0, 0, 0.5)
            ),
            url(https://www.trustyou.com/wp-content/uploads/2017/11/Umaid-Bhawan-Palace-Jodhpur-1.jpg);
          background-size: cover;
          font-family: 'Source Sans Pro', sans-serif;
        }
        header {
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          color: white;
          text-align: center;
        }
        h1 {
          text-transform: uppercase;
          margin: 0;
          font-size: 3rem;
          white-space: nowrap;
          color:white;
        }
        h3{
        color:white;
        }
        p {
          margin: 0;
          font-size: 1.5rem;
        }
        h2{
            color:black;
        }
        
label {
  padding: 0 1rem;
  display: inline-block;
  vertical-align: middle;
  line-height: 2.0rem;
  background: #2F2F2F;
  cursor: pointer;
  text-align: center;
  font-weight: 300;
  color: #fff;
  transition: color 0.2s ease-in-out, background 0.2s ease-in-out;
}
.center {
  margin-left: auto;
  margin-right: auto;
}
</style>
<meta charset="ISO-8859-1">
<title>search hotel result</title>
</head>

<body>
<%
String city=request.getParameter("city");
String checkin=request.getParameter("checkin");
String checkout=request.getParameter("checkout");
String noofrooms=new String(request.getParameter("noofrooms"));
String noofpeople=new String(request.getParameter("noofpeople"));

request.getSession(false).setAttribute("checkin",checkin);
request.getSession(false).setAttribute("checkout",checkout);
request.getSession(false).setAttribute("noofrooms",noofrooms);

try
{
Class.forName("com.mysql.jdbc.Driver");	
Connection connection =
DriverManager.getConnection("jdbc:mysql://localhost:3306/userregister", "root","swecha");
PreparedStatement ps=connection.prepareStatement("select hotel_name,address,hotel_id,amenities,total_no_of_rooms from hotels where city=? or state=? or address=?");
ps.setString(1,city.toUpperCase());
ps.setString(2,city.toUpperCase());
ps.setString(3,city.toUpperCase());
PreparedStatement psw=null;
ResultSet rsw=null;
ResultSet rs=ps.executeQuery();
out.println("<table class=center>");
int c=0;
while(rs.next())
{
	if(Integer.parseInt(rs.getString(5))>=Integer.parseInt(noofrooms))
	
	{
	   	
		psw=connection.prepareStatement("select count(room_id),room_type,price from room where hotel_id=? and ? not between checkin and checkout and ? not between checkin and checkout");
		psw.setString(1,rs.getString(3));
		psw.setString(2,checkin);
		psw.setString(3,checkout);
	
    	rsw=psw.executeQuery();
	
    	while(rsw.next())
	{
			if(c==0)
			{
				out.println("<tr><th>Hotel Name</th><th>City</th><th>Room Type</th><th>price</th><th> Hotel Adress</th><th>amenities</th><th>Number of avaliable rooms</th><th>Book Now</th></tr>");
			}
			c++;
		 //out.println("<tr><form><td>"+(new String(rs.getString(1))).trim()+"</td><td>"+city+"</td><td>"+rsw.getString(2)+"</td><td>"+rsw.getString(3)+"</td><td>"+rs.getString(2)+"</td><td>"+rs.getString(4)+"</td>"+"<td>"+rs.getString(5)+"</td><td><a href='guestform.jsp'><input type='button'value='Book'/></a></td></tr>");
			
		out.println("<tr><form action=loginRegister method=post><td>"+(new String(rs.getString(1))).trim()
				+"</td><td>"+city+"</td><td>"+rsw.getString(2)+"</td><td>"+rsw.getString(3)+"</td><td>"+rs.getString(2)+"</td><td>"+rs.getString(4)+"</td>"+"<td>"+rs.getString(5)+
				"</td><td> <input type=hidden name=hotelId value="+rs.getString(3)+" />  <input type=hidden name=price value='"+rsw.getString(3)+"' />  <input type=hidden name=availablerooms value='"+rs.getString(5)+"' /><button class=form__button type=submit value=Book name=submit>Book</button></td></form></tr>");

		
	}
		
	}
}
if(c==0)
{
	out.println("<h1>All rooms are booked or there is no rooms in given city/state</h1>");
}
}
catch(Exception a)
{
out.println(a);	
}
%>
</body>
</html>
