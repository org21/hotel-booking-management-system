<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title>Login / Sign Up Form</title>
    <link rel="shortcut icon" href="/assets/favicon.ico">
    <style>
   
        body {
           
          background-repeat: no-repeat;
          background-attachment: fixed;
          background-size: 100% 100%;
        
            --color-primary: #009579;
            --color-primary-dark: #007f67;
            --color-secondary: #252c6a;
            --color-error: #cc3333;
            --color-success: #4bb544;
            --border-radius: 4px;
        
            margin: 0;
            height: 100vh;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 18px;
            background: url(./background.jpg);
            background-size: cover;
        }
        
        .container {
            width: 400px;
            max-width: 400px;
            margin: 1rem;
            padding: 2.0rem;
            box-shadow: 0 0 40px rgba(0, 0, 0, 0.2);
            border-radius: var(--border-radius);
            background: #ffffff;
           
        }
        
        .container,
        .form__input,
        .form__button {
            font: 500 1rem 'Quicksand', sans-serif;
        }
        
        .form--hidden {
            display: none;
        }
        
        .form > *:first-child {
            margin-top: 0;
        }
        
        .form > *:last-child {
            margin-bottom: 0;
        }
        
        .form__title {
            margin-bottom: 1rem;
            text-align: center;
        }
        
        .form__message {
            text-align: center;
            margin-bottom: 1rem;
        }
        
        .form__message--success {
            color: var(--color-success);
        }
        
        .form__message--error {
            color: var(--color-error);
        }
        
        .form__input-group {
            margin-bottom: 1rem;
        }
        
        .form__input {
            display: block;
            width: 100%;
            padding: 0.5rem;
            box-sizing: border-box;
            border-radius: var(--border-radius);
            border: 1px solid #dddddd;
            outline: none;
            background: #eeeeee;
            transition: background 0.2s, border-color 0.2s;
        }
        
        .form__input:focus {
            border-color: var(--color-primary);
            background: #ffffff;
        }
        
        .form__input--error {
            color: var(--color-error);
            border-color: var(--color-error);
        }
        
        .form__input-error-message {
            margin-top: 0.5rem;
            font-size: 0.85rem;
            color: var(--color-error);
        }
        
        .form__button {
            width: 100%;
            padding: 0.5rem 1rem;
            font-weight: bold;
            font-size: 1.1rem;
            color: #ffffff;
            border: none;
            border-radius: var(--border-radius);
            outline: none;
            cursor: pointer;
            background: var(--color-primary);
        }
        
        .form__button:hover {
            background: var(--color-primary-dark);
        }
        
        .form__button:active {
            transform: scale(0.98);
        }
        
        .form__text {
            text-align: center;
            font-size: 1rem;
       
        }
        
        .form__link {
            color: var(--color-secondary);
            text-decoration: none;
            cursor: pointer;
             font-size: 0.5rem;
            
        }
        
        .form__link:hover {
            text-decoration: underline;
        }
        @import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,900);
        body {
          background: 
            linear-gradient(
              rgba(0, 0, 0, 0.5),
              rgba(0, 0, 0, 0.5)
            ),
            url(https://www.trustyou.com/wp-content/uploads/2017/11/Umaid-Bhawan-Palace-Jodhpur-1.jpg);
          background-size: cover;
          font-family: 'Source Sans Pro', sans-serif;
        }
        header {
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          color: white;
          text-align: center;
        }
        h1 {
          text-transform: uppercase;
          margin: 0;
          font-size: 3rem;
          white-space: nowrap;
          color:white;
        }
        h3{
        color:white;
        }
        p {
          margin: 0;
          font-size: 1.5rem;
        }
        h2{
            color:black;
        }
        
[class*="grid-"] {
  height: 2.0rem;
  font-size: 1rem;
}
label {
  padding: 0 1rem;
  display: inline-block;
  vertical-align: middle;
  line-height: 2.0rem;
  background: #2F2F2F;
  cursor: pointer;
  text-align: center;
  font-weight: 300;
  color: #fff;
  transition: color 0.2s ease-in-out, background 0.2s ease-in-out;
}
</style>
</head>
<body>
    <div class="container">
    <form action="loginRegister" method="post">           
            <h3 style=color:red>${message}</h3>

            <h2 class="form__title">Create an account</h2>
            <div class="form__message form__message--error"></div>
            <div class="form__input-group">
                <input type="text" class="form__input" autofocus placeholder="First Name" name="fname" required>
                <div class="form__input-error-message"></div>
            </div>
            <div class="form__input-group">
                <input type="text" class="form__input" autofocus placeholder="Last Name" name="lname" required>
                <div class="form__input-error-message"></div>
            </div>
            <div class="form__input-group">
                <input type="text" class="form__input" autofocus placeholder="Username"  name="username"  required>
                <div class="form__input-error-message"></div>
            </div>

            <div class="form__input-group">
                <input type="password" class="form__input" autofocus placeholder="Password" name="password1" required>
                <div class="form__input-error-message"></div>
            </div>
            <div class="form__input-group">
                <input type="date" class="form__input" autofocus placeholder="Date of Birth" name="dob" required>
                <div class="form__input-error-message"></div>
            </div>
            <div class="form__input-group">
                <input type="tel" class="form__input" autofocus placeholder="Contact Number" name="contact" required>
                <div class="form__input-error-message"></div>
            </div>
            <div class="form__input-group">
                <input type="email" class="form__input" autofocus placeholder="Email Address" name="email" required>
                <div class="form__input-error-message"></div>
            </div>
            
            <div class="form__input-group">
                <input type="text" class="form__input" autofocus placeholder="State" name="state" required>
                <div class="form__input-error-message"></div>
            </div>
     <div>
     <div>
    <label for="categories" class="grid-20">UAN</label>
    <select name="IdType" id="date" class="grid-80">
          <option value="" selected>Choose</option>
          <option value="Aadhar">Aadhar</option>
          <option value="PAN">PAN</option>
        </select>
        </div>
        <br>
        <div>
      <input type="text" class="form__input" autofocus placeholder="UAN Prefered Number" name="IdValue" required>
      </div>
    <div class="form__input-error-message"></div>
    </div>
    <button class="form__button" type="submit" value="register" name="submit">Register</button>
       <input type="hidden" name="submit" value="login"/>
       
       <div>
        <p class="form__text">
                <a href="login.jsp" class="form_link">Already have account? Login</a>
       </p>
       </div>
      </form>
    </div>
  <script>
          
  var GivenDate;
  var CurrentDate = new Date();
  GivenDate = new Date(GivenDate);
  if(GivenDate > CurrentDate)
  {
      alert('Given date is greater than the current date.');
  }else{
      alert('Given date is not greater than the current date.');
  }
</script> 
</body>
</html>