package login.hbms.registration;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import login.hbm.payments.Payment;
import login.hbm.payments.PaymentDAO;
import login.hbm.payments.PaymentDAOImpl;
import login.hbms.bookings.Booking;
import login.hbms.bookings.BookingDAO;
import login.hbms.bookings.BookingDAOImpl;
import login.hbms.guest.Guest;
import login.hbms.guest.GuestDAO;
import login.hbms.guest.GuestDAOImpl;
import login.hbms.hotels.Hotel;
import login.hbms.hotels.HotelDAO;
import login.hbms.hotels.HotelDAOImpl;

/**
 * Servlet implementation class LoginRegister
 */

@WebServlet("/loginRegister")

public class LoginRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginRegister() {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		CustomerDAO cd = new CustomerDAOImpl();
		Random random = new Random();

		String userName = request.getParameter("username");
		String password = request.getParameter("password1");
		String submitType = request.getParameter("submit");
		int status;

		System.out.println(userName + "" + password);

		Customer c = new Customer();
		c = cd.getCustomer(userName, password);
		System.out.println("Login admin" + c.getUsername());

		System.out.println("Submitted value: " + submitType);

		if (submitType.equals("adminLogin") && c != null && c.getFirstname() != null&& "admin".equals(c.getUsername())) {

			request.setAttribute("message", c.getFirstname());
			request.getRequestDispatcher("adminwelcome.jsp").forward(request, response);
		}
		else if (submitType.equals("search")) {

			// request.setAttribute("message",c.getFirstname());
			request.getRequestDispatcher("search.jsp").forward(request, response);
		}

		else if (submitType.equals("add")) {

			// request.setAttribute("message","Details added successfully");
			Guest g = new Guest();
			
			
			String BookingId = "BKID001" + random.nextInt(10000);
			request.getSession(false).setAttribute("BookingId", BookingId);
			g.setBookingId(BookingId);
			
			g.setFirstname(request.getParameter("fname"));
			g.setLastname(request.getParameter("lname"));
			g.setDateofbirth(request.getParameter("dob"));
			g.setContact(request.getParameter("contact"));
			g.setEmail(request.getParameter("email"));

			GuestDAO gd = new GuestDAOImpl();
			
			status = gd.insertGuest(g);
			System.out.println("status" + status);
			
			if (status == 1) {
				request.getRequestDispatcher("payments.jsp").forward(request, response);

			} else {
				request.setAttribute("message", "Error,Please enter correct value !!!");
				request.getRequestDispatcher("guestform.jsp").include(request, response);

			}
		}
		
		else if (submitType.equals("addpayment")) 
		{
			//request.getRequestDispatcher("paymentsuccess.jsp").forward(request, response);
			
			Payment p = new Payment();
			
			p.setCreditCard(request.getParameter("cardnum"));
			p.setBillAmount(request.getParameter("amount"));
			p.setBookingId((String) request.getSession(false).getAttribute("BookingId"));
			
			PaymentDAO pd=new PaymentDAOImpl();
			status=pd.insertPayment(p);
			System.out.println("Status : "+status);
			
			Booking b=new Booking();
			
			
			b.setUsername((String) request.getSession(false).getAttribute("username"));
			b.setBookingId((String) request.getSession(false).getAttribute("BookingId"));
			b.setCheckIn((String) request.getSession(false).getAttribute("checkin"));
			b.setCheckOut((String) request.getSession(false).getAttribute("checkout"));
			b.setNoofrooms((String) request.getSession(false).getAttribute("noofrooms"));
			Integer totalamount=Integer.valueOf((String) request.getSession(false).getAttribute("noofrooms"))*Integer.valueOf((String) request.getSession(false).getAttribute("price"));
			
			b.setHotelId((String) request.getSession(false).getAttribute("hotelId"));
			b.setTotalAmount(totalamount.toString());
			
            BookingDAO bd=new BookingDAOImpl();
            bd.insertBooking(b);
			
            Integer totalrooms=Integer.valueOf((String) request.getSession(false).getAttribute("availablerooms"))-Integer.valueOf((String) request.getSession(false).getAttribute("noofrooms"));
			
            HotelDAO hd=new HotelDAOImpl();
            hd.updateTotalNoOfRooms((String) request.getSession(false).getAttribute("hotelId"),totalrooms.toString());
            
            
			
			if (status == 1) {
				request.setAttribute("message",request.getSession(false).getAttribute("BookingId"));

				request.getRequestDispatcher("bookingsuccess.jsp").forward(request, response);

			} else {
				request.setAttribute("message", "Error,Please enter correct value !!!");
				request.getRequestDispatcher("payments.jsp").include(request, response);

			}
		
		}
		else if (submitType.equals("Book")) {
			// request.setAttribute("message","Details added successfully");
			request.getParameter("hotelId");
			
			System.out.println(request.getParameter("hotelId"));
			request.getParameter("price");
			System.out.println(request.getParameter("price"));
			request.getSession(false).setAttribute("availablerooms",request.getParameter("availablerooms"));
			
			request.getSession(false).setAttribute("hotelId",request.getParameter("hotelId"));
			request.getSession(false).setAttribute("price",request.getParameter("price"));
			
			request.getRequestDispatcher("guestform.jsp").forward(request, response);
		} else if (submitType.equals("addhotels")) {

			HotelDAO hd = new HotelDAOImpl();
			Hotel h = new Hotel();
			String hotelId = "HR001" + random.nextInt(10000);
			h.setHotelId(hotelId);

			h.setHotelName(request.getParameter("hname"));
			h.setGstNumber(request.getParameter("gst"));
			h.setAddress(request.getParameter("address"));
			h.setCity(request.getParameter("city"));
			h.setState(request.getParameter("state"));
			h.setRoomSize(request.getParameter("size"));
			h.setRoomType(request.getParameter("type"));
			h.setPrice(request.getParameter("price"));
			h.setAmenities(request.getParameter("amenities"));
			h.setTotal_Available_Rooms(request.getParameter("availablerooms"));
			hd.insertHotel(h);

			request.setAttribute("message", "Hotel Added Succesfully, Hotel Id is :" + hotelId);

			request.getRequestDispatcher("datasuccess.jsp").forward(request, response);

		} else if (submitType.equals("login") && c != null && c.getFirstname() != null) {

			request.setAttribute("message", c.getFirstname());
			request.getRequestDispatcher("welcomeuser.jsp").forward(request, response);
			request.getSession(true).setAttribute("username",userName);

		} else if (submitType.equals("register")) {

			c.setUAN(request.getParameter("IdValue"));
			c.setUsername(userName);
			c.setFirstname(request.getParameter("fname"));
			c.setLastname(request.getParameter("lname"));
			c.setPassword(password);
			c.setEmail(request.getParameter("email"));
			c.setContact(request.getParameter("contact"));
			c.setState(request.getParameter("state"));
			status = cd.insertCustomer(c);
			System.out.println("status" + status);

			if (status == 1) {
				request.setAttribute("message", "Registration Done, Please Login to Continue !!!");
				request.getRequestDispatcher("login.jsp").forward(request, response);
			} else if (status == -1) {
				// System.out.println("Error , Please Enter correct values");

				request.setAttribute("message", "Error,Please enter correct value !!!");

				request.getRequestDispatcher("register.jsp").include(request, response);

			}

		} else {

			request.setAttribute("message", "Data Not Found, Click on Register !!!");
			request.getRequestDispatcher("login.jsp").forward(request, response);

		}

	}
}
