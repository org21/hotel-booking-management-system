package login.hbm.payments;

import java.sql.Connection;
import java.sql.PreparedStatement;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PaymentDAOImpl implements PaymentDAO {
	static Connection con;
	static PreparedStatement ps;

	@Override
	public int insertPayment(Payment p) {
		int status=0;
		
		try {
			//System.out.println("add Customer...");
			
			con=MyConnectionProvider.getCon();	
			ps=con.prepareStatement("insert into payment(booking_id,cardnum,amount) VALUES (?, ?,?)");
			ps.setString(1, p.getBookingId());
			ps.setString(2,p.getCreditCard() );
			ps.setString(3, p.getBillAmount());
			
			status=ps.executeUpdate();
			con.close();	
			
			//	System.out.println(c.getUsername() + " added");
			}catch(Exception e) {
				status=-1;
				
				System.out.println(e);
			}
			return status;

	}

}
