/*
Database name userregister
*/

use userregister;

SELECT * FROM userregister.customer2;

/* adding admin credentials */
insert into customer2(first_name, last_name, username, password,dob,email,contact,state,UAN) VALUES ('admin', 'admin', 'admin', 'password',NULL,'admin@gmail.com','9813171222','AP','1729712472');

/*
Hotel table
*/

create table hotels(
hotel_id varchar(20) NOT NULL ,
hotel_name varchar(30),
hotel_gst_no varchar(30),
address varchar(40),
city varchar(15),
state varchar(20),
room_type VARCHAR(25) NOT NULL,
room_size varchar(10),
price varchar(20),
amenities varchar(100),
total_no_of_rooms int(4)
);

select * from hotels;

select * from room;

select * from guest;

drop table hotels;

create table hotels(

hotel_name varchar(30),
hotel_id varchar(20) NOT NULL,
address varchar(40),
state varchar(100),
city varchar(15),
hotel_gst_no varchar(30),
amenities varchar(100),
total_no_of_rooms int(10),
room_type VARCHAR(25) NOT NULL,
room_size varchar(10),
price varchar(20)
);


insert into hotels values('ranga hotels','H001','hc nagar','Andhra Pradesh','Krishna',245621785,'Wifi Connection',10,'1B','100sqft','2000');
insert into hotels values('Shuba hotels','H002','anigandlapadu','Andhra Pradesh','Krishna',245621786,'healthy food',5,'3B','100sqft','3000');
insert into hotels values('Militry hotel','H003','Nandigama','Andhra Pradesh','Krishna',245622787,'Safety Standards',25,'2B','1000sqft','5000');


insert into hotels values('Catch Hotels','H004','Kamapellama','Delhi','ongole',245621256,'Less junk',32);
insert into hotels values('Soda House','H005','Greater Noida','Gujarath','Noida',245627225,'Healthy standards',42);
insert into hotels values('Pista house','H006','Beggupeta','Telegana','Hyderbad',242227856,'Rich Proients',5);
insert into hotels values('KFC','H007','Pajjaguta','Telegana','Hyderbad',24228756,'Tasty Food',25);

/* Room Table */

create table room(room_id varchar(10),hotel_id varchar(20),room_type varchar(30),room_size int,price int,checkin varchar(20),checkout varchar(20));
/*
insert into room values('R001','H001','1A',2,550000,'12/2/2021','18/2/2021');
insert into room values('R002','H001','2A',3,100000,'12/2/2021','19/2/2021');
insert into room values('R003','H002','3A',5,250000,'14/2/2021','18/2/2021');
insert into room values('R004','H003','1A',3,10000,'16/2/2021','20/2/2021');
insert into room values('R005','H005','3A',2,200000,'24/07/2021','25/07/2021');
insert into room values('R006','H006','1A',3,250000,'23/08/2021','27/08/2021');
insert into room values('R007','H006','2A',4,260000,'28/08/2021','30/08/2021');
*/

drop table room;


create table room(room_id varchar(10),hotel_id varchar(20),room_type varchar(30),room_size varchar(20),price int,checkin varchar(20),checkout varchar(20));

select * from room;

select * from hotels;


select count(room_id),room_type,price from room where hotel_id='HR0012128';

insert into room values('R001','HR0018157', '1B', '1000sroomqft', '2000','24/07/2021','25/07/2021');
insert into room values('R002','HR0018487', '2B', '2000sqft', '3000','23/08/2021','27/08/2021');
insert into room values('R003','HR0013300',  '2B', '2000sqft', '3000', '28/08/2021','30/08/2021');
insert into room values('R004','HR0017405',  '1B', '1000sqft', '1000', '2/08/2021','4/08/2021');
insert into room values('R005','HR0017176', '3B', '3000sqft', '6000', '4/08/2021','5/08/2021');

drop table guest;

select * from guest;
delete from guest where email=NULL;

select * from payment;

drop table payment;

create table guest(
guest_id INT(3) NOT NULL AUTO_INCREMENT PRIMARY KEY,
booking_id varchar(10),
first_name varchar(20),last_name varchar(20),
dateofbirth date,email varchar(20),contact varchar(20));

create table payment(
payment_id INT(3) NOT NULL AUTO_INCREMENT PRIMARY KEY,
booking_id varchar(10),
cardnum varchar(16),
amount varchar(10));


CREATE TABLE bookings(username varchar(25),booking_id varchar(20),hotel_id varchar(20),noofrooms int,total_amount varchar(10),checkin varchar(20), checkout varchar(20));

select * from bookings;

select payment.booking_id,hotels.hotel_name,hotels.amenities from payment,hotels;

select * from room;
drop table bookings;

select * from hotels;

select * from guest;

select * from bookings;

select * from payment;

select * from room;

select * from bookings;

select bookings.booking_id,hotels.hotel_name,bookings.noofrooms,room.room_type,payment.amount,hotels.amenities 
from payment,hotels,room,bookings where bookings.username='raju123' and bookings.hotel_id=hotels.hotel_id and
hotels.hotel_id=room.hotel_id and bookings.booking_id=payment.booking_id;

/*
Register & Login user table
*/
select * from customer2;  

/*
Hotel details table
*/

select * from hotels;


delete from hotels where hotel_gst_no='HR001195'; 
/*
Room table
*/
select * from room;

/*
Guest table
*/ 

select * from guest;

/*
payment table
*/

select * from payment;


/*
Bookings table
*/

select * from bookings;




