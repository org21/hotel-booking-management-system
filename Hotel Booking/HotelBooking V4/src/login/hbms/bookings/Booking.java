package login.hbms.bookings;

public class Booking {

	private String BookingId;
	private String HotelName;
	private String NoOfRooms;
	private String RoomType;
	private String TotalAmount;
	private String Amenities;
	
	
	public String getBookingId() {
		return BookingId;
	}
	public void setBookingId(String bookingId) {
		BookingId = bookingId;
	}
	public String getHotelName() {
		return HotelName;
	}
	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}
	public String getNoOfRooms() {
		return NoOfRooms;
	}
	public void setNoOfRooms(String noOfRooms) {
		NoOfRooms = noOfRooms;
	}
	public String getRoomType() {
		return RoomType;
	}
	public void setRoomType(String roomType) {
		RoomType = roomType;
	}
	public String getTotalAmount() {
		return TotalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		TotalAmount = totalAmount;
	}
	public String getAmenities() {
		return Amenities;
	}
	public void setAmenities(String amenities) {
		Amenities = amenities;
	}

}
