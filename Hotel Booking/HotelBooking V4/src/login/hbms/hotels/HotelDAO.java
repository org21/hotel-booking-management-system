package login.hbms.hotels;

public interface HotelDAO {

	public int insertHotel(Hotel h);
  
	public Hotel getHotel(String City,String Checkin,String Checkout,String NoOfRooms,String NoOfPeople); 
	public int insertHotelRoom(Hotel h);
	
	}

