package login.hbms.hotels;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;

import login.hbms.registration.Customer;
import login.hbms.registration.MyConnectionProvider;

public class HotelDAOImpl implements HotelDAO {

	static Connection con;
	static PreparedStatement ps;
	
	@Override
	public int insertHotel(Hotel h) {
       int status1=0;
      try {
    	con=MyConnectionProvider.getCon();
    	
   		ps=con.prepareStatement("insert into hotels(hotel_id,hotel_name,hotel_gst_no,address,city,state,amenities,total_no_of_rooms)"
    	+ "values(?,?,?,?,?,?,?,?)");
   		
   		ps.setString(1, h.getHotelId());
   		ps.setString(2, h.getHotelName());
   		ps.setString(3, h.getGstNumber());
   		ps.setString(4, h.getAddress());
   		ps.setString(5, h.getCity());
   		ps.setString(6, h.getState());
   		//ps.setString(7, h.getRoomType());
   		//ps.setString(8, h.getRoomSize());
   		//ps.setString(9, h.getPrice());
   		ps.setString(7, h.getAmenities());
   		ps.setString(8, h.getTotal_Available_Rooms());
   		
   		status1=ps.executeUpdate();
   		con.close();
   		insertHotelRoom(h);
   		
       }catch(Exception e) {
    	   System.out.println(e);
       }
		return status1;
	}
	@Override
	public int insertHotelRoom(Hotel h) {
       int status1=0;
       
      try {
    	con=MyConnectionProvider.getCon();
    	Random random = new Random();
		String roomid = "R001" + random.nextInt(1000);

   		ps=con.prepareStatement("insert into room(room_id,hotel_id,room_type,room_size,price,checkin,checkout)"
    	+ "values(?,?,?,?,?,?,?)");
   		
   		ps.setString(1, roomid);
   	
   		ps.setString(2, h.getHotelId());
   		ps.setString(3, h.getRoomType());
   		ps.setString(4, h.getRoomSize());
   		ps.setString(5, h.getPrice());
   		ps.setString(6, "15/07/2021");
   		ps.setString(7, "15/09/2021");
   		
   		status1=ps.executeUpdate();
   		con.close();
   		
       }catch(Exception e) {
    	   System.out.println(e);
       }
		return status1;
	}
	
	
	@Override
	public Hotel getHotel(String City, String Checkin, String Checkout, String NoOfRooms, String NoOfPeople) {

		
		Hotel h=new Hotel();
		
		return null;
	}
	public void updateTotalNoOfRooms(String hotelId,String availableRooms) {
		 
		
	      try {
	    	con=MyConnectionProvider.getCon();
	    	
			
	    
	   		ps=con.prepareStatement("update hotels set total_no_of_rooms=? where hotel_id=?");
	    	
	   		ps.setString(1, availableRooms);
	   		ps.setString(2, hotelId);
	   		
	   		
	   		ps.executeUpdate();
	   		con.close();
	   		
	       }catch(Exception e) {
	    	   System.out.println(e);
	       }
		
		
	}
	
	
}
