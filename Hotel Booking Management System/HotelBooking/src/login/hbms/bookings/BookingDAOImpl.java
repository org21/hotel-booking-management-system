package login.hbms.bookings;

import java.sql.Connection;
import java.sql.PreparedStatement;

import login.hbms.registration.MyConnectionProvider;

public class BookingDAOImpl implements BookingDAO {

	static Connection con;
	static PreparedStatement ps;
	@Override
	public int insertBooking(Booking b) {

		 int status1=0;
	      try {
	    	con=MyConnectionProvider.getCon();
	    		    	
	   		ps=con.prepareStatement("insert into bookings(username,booking_id,hotel_id,noofrooms,total_amount,checkin,checkout)"
	    	+ "values(?,?,?,?,?,?,?)");
	   		
	   		ps.setString(1,b.getUsername());
	   		ps.setString(2, b.getBookingId());
	   		ps.setString(3, b.getHotelId());
	   		ps.setString(4,b.getNoofrooms());
	   		ps.setString(5, b.getTotalAmount());
	   		ps.setString(6, b.getCheckIn());
	   		ps.setString(7, b.getCheckOut());
	   		
	   		status1=ps.executeUpdate();
	   		con.close();
	   		
	   	//	insertHotelRoom(b);
	   		
	       }catch(Exception e) {
	    	   System.out.println(e);
	       }
			return status1;
		
	}

}
