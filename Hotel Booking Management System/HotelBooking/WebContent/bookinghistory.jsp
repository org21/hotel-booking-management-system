<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Bookings</title>
<style>

body {
  font-family: "Open Sans", sans-serif;
  line-height: 1.25;
}

table {
  border: 1px solid #ccc;
  border-collapse: collapse;
  margin: 0;
  padding: 0;
  width: 50%;
  table-layout: fixed;
}
table tr {
  background-color: #f8f8f8;
  border: 1px solid #ddd;
  padding: .25em;
}

table th,
table td {
  padding: .625em;
  text-align: center;
}
table {
  width: 70%;
  
}
	
@media screen and (max-width: 500px) {
  table {
    border: 0;
  }

  table caption {
    font-size: 1.3em;
  }

}

@import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,900);
        body {
          background: 
            linear-gradient(
              rgba(0, 0, 0, 0.5),
              rgba(0, 0, 0, 0.5)
            ),
            url(https://www.trustyou.com/wp-content/uploads/2017/11/Umaid-Bhawan-Palace-Jodhpur-1.jpg);
          background-size: cover;
          font-family: 'Source Sans Pro', sans-serif;
        }
        header {
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          color: white;
          text-align: center;
        }
        h1 {
          text-transform: uppercase;
          margin: 0;
          font-size: 3rem;
          white-space: nowrap;
          color:white;
        }
        h3{
        color:white;
        }
        p {
          margin: 0;
          font-size: 1.5rem;
        }
        h2{
            color:black;
        }

.center {
  margin-left: auto;
  margin-right: auto;
}

body 
{
  font-family: Arial, Helvetica, sans-serif;
}
.navbar {
  overflow: hidden;
  background-color: rgba(214, 135, 16, 0.884);
}
.navbar a {
  float: left;
  font-size: 16px;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}
.dropdown {
  float: right;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color: red;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: rgb(0, 0, 0);
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}
.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}

header {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: white;
  text-align: center;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #e6b413;
  color: white;
}

.topnav-right {
  float: right;
}
        .myTableStyle
        {
           position:absolute;
           top:50%;
           left:60%; 

            /*Alternatively you could use: */
           /*
              position: fixed;
               bottom: 50%;
               right: 50%;
           */
  }

</style>
</head>
<body style="background-color:rgb(90, 218, 235)">
  <div class="topnav">
  <a class="active" href="welcomeuser.jsp">Home</a>
  <div class="topnav-right">
      <a href="index.jsp">Log out</a>
      </div>
</div>

<%
try
{
Class.forName("com.mysql.jdbc.Driver");	
Connection connection =
DriverManager.getConnection("jdbc:mysql://localhost:3306/userregister", "root","swecha");
PreparedStatement ps=connection.prepareStatement("select bookings.booking_id,hotels.hotel_name,bookings.noofrooms,room.room_type,payment.amount,hotels.amenities from payment,hotels,room,bookings where bookings.username=? and bookings.hotel_id=hotels.hotel_id and hotels.hotel_id=room.hotel_id and bookings.booking_id=payment.booking_id");
ps.setString(1,(String) request.getSession(false).getAttribute("username"));

ResultSet rs=ps.executeQuery();
out.println("<table class=center id=myTable border>");
int c=0;

out.println("<tr><th>Booking Id</th><th>Hotel Name</th><th>Number of Rooms</th><th>Room Type</th><th> Total Amount</th><th>Amenities</th>");

while(rs.next())
{
						
   out.println("<tr><td>"+rs.getString(1)+"</td><td>"+rs.getString(2)+"</td><td>"+rs.getString(3)+"</td><td>"+rs.getString(4)+"</td><td>"+rs.getString(5)+"</td><td>"+rs.getString(6)+"</td></tr>");
		
}
}
catch(Exception a)
{
out.println(a);	
}
%>
    
</body>
</html>