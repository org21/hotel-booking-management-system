# Hotel Booking Management System

## Description-
  This Website allows users for managing the hotel booking activities such as adding a hotel details, searching the hotel details and booking, adding the guest or traveler’s details and payment.Customer needs to create his account first in the website and then he can do booking online. It also provides details about various hotels and amenities.  
   
### It includes :
  - Sign Up Form
  - Log In Form
  - homepage
  - Search Details
  - Room booking
  - Guest Details
  - Booking History

## Tech Stack Using-

### 1. Front end Technologies:
  - HTML
  - CSS
  - Javascript
  - Bootstrap
  
### 2. Back end Technologies:
  - Java and Servlet
  
### 3. Database:
  - MySQL
  
### 4. Webserver:
  - Tomcat


## Software Downloads:

- Eclipse IDE (Open Source):
    
  https://www.eclipse.org/downloads/

- Tomcat Server

   https://tomcat.apache.org/download-80.cgi

#### Reference: https://www.youtube.com/watch?v=fPjgGub33As

- Mysql Connector & Dependencies:

  https://github.com/RameshMF/servlet-tutorial/tree/master/jsp-servlet-jdbc-mysql-example/WebContent/WEB-INF/lib

- MySQL Workbench

  https://dev.mysql.com/downloads/workbench/

- Git

  https://git-scm.com/downloads

- Visual studio Code

  https://code.visualstudio.com/download


## Functional Requirements:

o	Register User and Login – User can provide the required details to register in the system and validate the user credentials.

o	Add Hotel Details – Hotel Administrators add the hotel, room and price related details into the system.

o	Search Hotel Details – User can search the hotel and room related details for specific check-in and check-out dates and view the list of hotels available for them. 

o	Book the hotel and Payment Details – Book the room by entering the corresponding guest names and other related details and provide the payment details.

o	View Booking History – The user should be able to view the previously booked hotel details.



Project Hotel Booking Management System Contains:

-> Hotel Booking Folder (Eclipse Project)
	- Includes Code Logic Files follwed with MVC Architecture, Web Content Files for UI improvement
-> Output Screenshots 
        - Includes each and every module output page
-> Database
	- Includes Table Queries which are used in the project


Credits:

## Team:

Santhosh
Naga Malleswari
Akhil
Raju
Vyshnavi
Aneera

## Mentors:
 - Anuj
 - Ranjith(Supporting Mentor)
 - Rengananthan (Reviewer)

Other Resources:

https://codepen.io/
https://www.youtube.com/watch?v=7bl8UrkYbaU
https://stackoverflow.com/
github

and Many sources helped us in order to complete this whole project. 