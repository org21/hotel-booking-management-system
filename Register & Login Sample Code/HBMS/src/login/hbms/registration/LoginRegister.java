package login.hbms.registration;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginRegister
 */

@WebServlet("/loginRegister")

public class LoginRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public LoginRegister() 
    {
    
    	
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	
    	CustomerDAO cd=new CustomerDAOImpl();
    	
    	String userName=request.getParameter("username");
    	String password=request.getParameter("password1");
    	String submitType=request.getParameter("submit");
    	
    	Customer c=new Customer();
        c=cd.getCustomer(userName, password);
    	
    	if(submitType.equals("login") && c!=null && c.getName()!=null) {

    		request.setAttribute("message",c.getName());
    		request.getRequestDispatcher("welcome.jsp").forward(request, response);
    		
    	}else if(submitType.equals("register")) {
    		
    		c.setUsername(userName);
    		c.setName(request.getParameter("name"));
    		c.setPassword(password);
    		cd.insertCustomer(c);
    		
    		request.setAttribute("message", "Registration Done, Please Login to Continue !!!");
     	    request.getRequestDispatcher("login.jsp").forward(request, response);    	    
    		
    	}else {
    	    request.setAttribute("message", "Data Not Found, Click on Register !!!");
    	    request.getRequestDispatcher("login.jsp").forward(request, response);
    	    
    	}
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
	}

}
